<?php

const ADAVANZO_MAX_CLASSES =  1000;
const ADAVANZO_MAX_ITERATIONS =  10;

define('ADAVANZO_STARTED_AT', microtime(true));

$total_time = 0;

function adavanzo_load_class(string $class_name, int $a)
{
  if (!class_exists($class_name))
  {
    require_once "lib/class_$a.php";
  }
}

function adavanzo_save_result(float $finished_at)
{
  global $total_time;
  $total_time += $finished_at - ADAVANZO_STARTED_AT;
}

function adavanzo_print_result()
{
  global $total_time;
  echo sprintf("Avg execution time %s\n", $total_time / ADAVANZO_MAX_ITERATIONS);
}
