<?php

include 'env.php';

for ($i = 1; $i <= ADAVANZO_MAX_ITERATIONS; $i++) {
  for ($a = 1; $a <= ADAVANZO_MAX_CLASSES; $a++) {
    require_once "lib/class_$a.php";
  }
}

adavanzo_save_result(microtime(true));
adavanzo_print_result();

